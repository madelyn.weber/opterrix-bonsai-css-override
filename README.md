# Opterrix-Bonsai CSS Override

CSS override for re-skinning Opterrix in Bonsai colors and adding Bonsai's logo

## Steps for Implementing CSS Override in Chrome

1. Allow overrides in Opterrix through Google Chrome (For this, we followed steps 1-4 on this site: https://www.trysmudford.com/blog/chrome-local-overrides/) At step 3, select any folder on your device that Chrome can have permission to access.

2. The css file to be replaced can be found under Sources > Page Tab> top folder > app.opterrix.com > build > main-a9faa4bbc4c283b10ded.css. Then right click on that file and select 'Save for Overrides.'

3. Then after overwriting the contents of that css file with the file attached below, it should save in chrome.

## Troubleshooting Common Problems

If, when opening Opterrix, the local overrides are not taking effect, right click on the page and select "Inspect" from the menu. Once the inspect window is open, if the overrides then load, close the inspect window and the overrides should remain in place.
